export const state = function () {
    return {
        press_arr: [
            {
                id: 1,
                link: '',
                img: '/static/about/1.jpg',
                hover: '/static/about/1_hover.png'
            },
            {
                id: 2,
                link: '',
                img: '/static/about/2.jpg',
                hover: '/static/about/2_hover.jpg'
            },
            {
                id: 3,
                link: '',
                img: '/static/about/3.png',
                hover: '/static/about/3_hover.png'
            },
            {
                id: 4,
                link: '',
                img: '/static/about/4.png',
                hover: '/static/about/3_hover.png'
            },
            {
                id: 5,
                link: '',
                img: '/static/about/5.png',
                hover: '/static/about/5_hover.png'
            },
            {
                id: 6,
                link: '',
                img: '/static/about/6.png',
                hover: '/static/about/5_hover.png'
            },
            {
                id: 7,
                link: '',
                img: '/static/about/7.jpg',
                hover: '/static/about/7_hover.jpg'
            },
            {
                id: 8,
                link: '',
                img: '/static/about/8.jpg',
                hover: '/static/about/1_hover.png'
            },
            {
                id: 9,
                link: 'https://www.admagazine.ru/interior/polukruglaya-kvartira-v-dome-1933-goda-68-m',
                hover: '/static/about/1_hover.png',
                img: '/static/about/9.png'
            },
            {
                id: 10,
                link: 'https://www.elledecoration.ru/interior/flats/eklektichnaya-dvukhurovnevaya-kvartira-s-estetikoi-1970-kh/',
                img: '/static/about/elle_pub.jpg',
                hover: '/static/about/elle_hover.png'
            },
            {
                id: 11,
                link: 'https://mydecor.ru/interior/flats/sovremennaya-kvartira-s-klassicheskimi-elementami-v-moskve/',
                img: '/static/about/9.jpg',
                hover: '/static/about/9_hover.png'
            }
        ],

        press_public_arr: [
            {
                id: 1,
                link: '',
                img: '/static/about/pub1.jpg'
            },
            {
                id: 2,
                link: '',
                img: '/static/about/pub2.jpg'
            },
            {
                id: 3,
                link: '',
                img: '/static/about/pub3.jpg'
            },
            {
                id: 4,
                link: '',
                img: '/static/about/pub4.jpg'
            },
            {
                id: 5,
                link: 'https://addawards.ru/winners/2020/150172/',
                img: '/static/about/pub5.jpg'
            }
        ]
    }
}
