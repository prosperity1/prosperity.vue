up:
	docker-compose up -d app app_static

down:
	docker-compose down

restart: down up

install:
	docker-compose run app yarn install
