export default {
    components: [
        '~/components', // default level is 0
        { path: 'node_modules/vue-picture-swipe/src', level: 1 },
    ],
    // components: true,
    server: {
        // port: 8081, // default: 3000
        port: 3000, // default: 3000
        host: '0.0.0.0', // default: localhost
    },
    mode: 'universal',
    /*
    ** Headers of the page
    */
    head: {
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.png'}
        ],
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' }
        ]
    },
    /*
    ** Customize the progress-bar color
    */
    loading: {color: '#fff'},
    /*
    ** Global CSS
    */
    css: [
        // '@/assets/main.css',
        // '@/assets/about.css'
        // 'vue-ssr-carousel/index.css'
    ],
    layoutTransition: {
      mode: 'in-out'
    },
    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        {
            src: '~/plugins/video-background',
            ssr: false
        }
    ],
    /*
    ** Nuxt.js dev-modules
    */
    buildModules: [
        'vue-ssr-carousel/nuxt'
    ],
    /*
    ** Nuxt.js modules
    */
    modules: [],
    /*
    ** Build configuration
    */
    build: {
        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {
        }
    }
}



